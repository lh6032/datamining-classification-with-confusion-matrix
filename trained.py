import pandas as pd
import numpy as np
test_df = pd.read_csv('../Abominable_Validation_Data_for_Testing_v2205.csv')
attribute_points = test_df['Ht'].values
results = []
for each_point in attribute_points:
    if ('Ht' == 'Age' and each_point <= 145) or ('Ht' == 'Ht' and each_point >= 145):
        results.append(+1)
        print('+1')
    else:
        results.append(-1)
        print('-1')
with open('results.csv', 'ab') as file_out:
    comment = 'Use Ht as Threshold:'
    np.savetxt(file_out, results, fmt='%d', delimiter=',', header=comment)
