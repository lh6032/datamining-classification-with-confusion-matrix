"""Author: Lingmin Hou, Sophie(lh6032) """

import pandas as pd
import numpy as np
import math
import matplotlib.pyplot as plt
import seaborn as sns
import os


def test_trained_threshold(test_file_path, best_threshold, attribute):
    """Creates trained program to read file with testing data and uses
    threshold to predict the classifier and write out results to csv file.
    """
    # create trained program
    with open('trained.py', 'w') as file:  
        # import packages
        file.write("import pandas as pd\n") 
        file.write("import numpy as np\n")
        # read file with tesing data
        file.write("test_df = pd.read_csv('" + test_file_path + "')\n")
        # get the target attribute column values
        file.write("attribute_points = test_df['" + attribute + "'].values\n")
        # use results to store predicted classifier
        file.write("results = []\n")
        # compare each point of the attribute column with the threshold
        file.write("for each_point in attribute_points:\n")
        # if use 'Age' as attribute and the point is less than or equal to the threshold, then that point belongs to classA, print +1
        # if use 'Ht' as attribute and the point is larger than the threshold, then that point belongs to classB, print -1
        file.write("    if ('" + attribute + "' == 'Age' and each_point <= " + str(best_threshold) + ") or ('" + attribute + "' == 'Ht' and each_point >= " + str(best_threshold) + "):\n")
        file.write("        results.append(+1)\n")   # store predicted result in results
        file.write("        print('+1')\n")
        file.write("    else:\n")
        file.write("        results.append(-1)\n")   # store predicted result in results
        file.write("        print('-1')\n")
        # write out results inside a csv file
        file.write("with open('results.csv', 'ab') as file_out:\n")
        file.write("    comment = 'Use " + attribute + " as Threshold:'\n")
        file.write("    np.savetxt(file_out, results, fmt='%d', delimiter=',', header=comment)\n")

def run_test_program():
    """Creates command to run the trained program.
    """
    os.system("python trained.py")


def get_Threshold(train_df, attribute, quant_attribute, bin_size):
    # get all possible thresholds, which are range from the min value of that attribute to max value of that attribute
    possible_thresholds = [value for value in range(math.floor(min(train_df[attribute])), math.ceil(max(train_df[attribute])))]
    # quantize the attribute column by round and store as a new column in the dataframe
    train_df[quant_attribute] = (train_df[attribute]/bin_size).apply(np.round) * bin_size 
    # print(train_df)
    best_cost_func = float('INF')
    index = 0
    all_cost_funcs = {}     # use dictionary to store all cost functions
    FP_rate = []    # use list to keep all FP rates
    TP_rate = []    # use list to keep all TP rates
    all_attribute_points = train_df[attribute].values
    all_class_points = train_df['Class'].values
    for each_threshold in possible_thresholds:
        TP = TN = FP = FN = 0
        for each_point_idx in range(0, len(all_attribute_points)):
            # if attribute is 'Age', then use 'Age' < threshold to judge
            if attribute == 'Age':
                condition = (all_attribute_points[each_point_idx] < each_threshold) 
            # if attribute is 'Ht', then use 'Ht' > threshold to judge
            else:
                condition = (all_attribute_points[each_point_idx] > each_threshold) 
            
            if condition:  # if 'Age' < threshold;   or if 'Ht' > threshold 
                if all_class_points[each_point_idx] == 1:  
                    TP += 1     # if belongs to classA, True Positive increases
                else:
                    FP += 1     # if belongs to classB, False Positive increases
            else:        # if 'Age' >= threshold;  or if 'Ht' <= threshold 
                if all_class_points[each_point_idx] == 1:
                    FN += 1   # if belongs to classA, False Negative increases
                else:
                    TN += 1  # if belongs to classB, True Negative increases
        all_cost_funcs[each_threshold] = FP + FN    # store cost function with each threshold in dictionary
        each_cost_func = FP + FN        # get cost function
        # find the minimize cost function and corresponding threshold and index
        if each_cost_func <= best_cost_func:
            best_cost_func = each_cost_func
            best_threshold = each_threshold
            best_index = index
        FP_rate.append(FP/(FP+TN))  # store FP rate
        TP_rate.append(TP/(TP+FN))  # store TP rate
        index += 1  
    print("Use " + attribute + " as Threshold:")
    print('best_cost_func =', best_cost_func)
    print('best_threshold=', best_threshold)
    print('best_index=', best_index)
    plot_cost_func(all_cost_funcs, attribute, 'Cost Function', best_threshold, best_cost_func)  # plot cost function
    plot_roc(TP_rate, FP_rate, attribute, best_threshold, best_index)   # plot ROC 
    return best_threshold

def plot_cost_func(all_cost_funcs, x_label, y_label, best_threshold, best_cost_func):
    """Plots all of cost functions corresponding to each threshold. Uses threshold as x, and cost function as y.
    """
    sorted_cost_func = sorted(all_cost_funcs.items()) # sort dictionary by key
    x, y = zip(*sorted_cost_func) # get x and y
    plt.plot(x, y)   # plot (x,y)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.scatter(best_threshold, best_cost_func) # plot point x=best_threshold, y=best_cost_func
    plt.annotate('  best_threshold=' + str(best_threshold) + '\n  best cost function=' + str(best_cost_func), (best_threshold, best_cost_func)) # text
    plt.show()


def plot_roc(TP_rate, FP_rate, attribute, best_threshold, best_index):
    """Plots ROC curve by using FP_rate as x and TP_rate as y, and indicate the best threshold location.
    """
    fig = plt.figure()
    ax = fig.add_subplot(111)
    plt.plot(FP_rate, TP_rate, '-o', fillstyle='none')  # plot all (x,y)
    ax.set_aspect('equal')   # make ax square
    plt.scatter(FP_rate[best_index], TP_rate[best_index], marker='s', color='red')  # plot the point with minimum cost function
    # annotate the point 
    ax.annotate('\nFP rate=' + str(FP_rate[best_index]) + '\nTP rate=' + str(TP_rate[best_index]), xy=(FP_rate[best_index], TP_rate[best_index]), xytext=(0.4, 0.7),
            arrowprops=dict(facecolor='black', shrink=0.1))
    plt.xlabel("False Positive Rate")
    plt.ylabel("True Positive Rate")
    if attribute == 'Age':
        plt.text(0.5, 0.5, 'Threshold =' + str(best_threshold) + ' years old')  # plot text if atribute is 'Age'
    else:
        plt.text(0.5, 0.5, 'Threshold =' + str(best_threshold) + '  cm')    # plot text if atribute is 'Ht'
    plt.title("ROC Curve by " + attribute + " Threshold")
    plt.show()


def main():
    # 2 files path
    train_file_path = "../Abominable_Training_Data_v2205.csv"
    test_file_path = "../Abominable_Validation_Data_for_Testing_v2205.csv"

    # read training data as dataframe
    train_df = pd.read_csv(train_file_path)
    # Use attribute 'Age' as threshold
    best_age_threshold = get_Threshold(train_df, 'Age', 'Quantize_Age', 2)    # get best age threshold
    test_trained_threshold(test_file_path, best_age_threshold, 'Age')     # test on testing data
    run_test_program()  # run trained program
    # Use attribute 'Ht' as threshold
    best_ht_threshold = get_Threshold(train_df, 'Ht', 'Quantize_Ht', 5)     # get best ht threshold
    test_trained_threshold(test_file_path, best_ht_threshold, 'Ht')     # test on testing data
    run_test_program()  # run trained program

    
if __name__ == '__main__':
    main()


